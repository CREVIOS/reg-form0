
var groupsByCategory = {
    A: ["Select", "Class 6", "Class 7", "Class 8"],
    B: ["Select", "Class 9", "Class 10"],
    C: ["Select", "Class 11", "Class 12"]
}

    function changecat(value) {
        if (value.length == 0) document.getElementById("category").innerHTML = "<option></option>";
        else {
            var catOptions = "";
            for (categoryId in groupsByCategory[value]) {
                catOptions += "<option>" + groupsByCategory[value][categoryId] + "</option>";
                
            }
            document.getElementById("category").innerHTML = catOptions;
        }
    }

